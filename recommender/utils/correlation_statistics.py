import logging
import numpy as np
import matplotlib.pyplot as plt
from brokenaxes import brokenaxes
import sys
import random
from recommender.config import config as conf
from recommender.datareader import prepare_data as prep
from recommender.recommendation_algorithms import memory_based_algorithms as algo


logger = logging.getLogger()


def _init_logging(logger, log_to_stdout = False):
    LOG_FORMAT = "%(asctime)s - %(levelname)s - %(name)s - %(message)s"

    log_filepath = 'mem-rec-corr-statistics.log'
    file_handler = logging.FileHandler(log_filepath)
    formatter = logging.Formatter(LOG_FORMAT)
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.INFO)
    logger.addHandler(file_handler)
    logger.setLevel(logging.INFO)

    if log_to_stdout:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(logging.DEBUG)
        stdout_handler.setFormatter(formatter)
        logger.addHandler(stdout_handler)


def item_to_item(config):
    df, rating_mat, played_mat, avg_rating_mtx, popularity_list = prep.create_datasets(config)
    shape_value = 1
    item_corrs = np.zeros((rating_mat.shape[shape_value], rating_mat.shape[shape_value]))
    for program in range(played_mat.shape[shape_value]):
        if program % 10 == 0:
            logger.info("Done %s programs out of %s.", program, played_mat.shape[shape_value])
        item_corrs[:, program] = algo.get_correlations(config, program, rating_mat, played_mat, avg_rating_mtx)

    item_corrs_flat = item_corrs.flatten()

    numbins = 50
    bax = brokenaxes(ylims=((0, 200000), (1750000, 1850000)))
    bax.hist(item_corrs_flat, numbins, histtype='bar')
    plt.savefig('corrs_item_to_item.png', bbox_inches='tight')


def user_to_user(config):
    df, rating_mat, played_mat, avg_rating_mtx, popularity_lit = prep.create_datasets(config)
    datamin = -1
    datamax = 1
    numbins = 50
    mybins = np.linspace(datamin, datamax, numbins + 1)
    myhist = np.zeros(numbins, dtype='int32')
    samples = 50000
    random_range = list(range(samples))
    random.shuffle(random_range)
    for i, user in enumerate(random_range):
        corrs = algo.get_correlations(config, user, rating_mat, played_mat, avg_rating_mtx)
        corrs = np.squeeze(np.asarray(corrs))
        htemp, bin_edges = np.histogram(corrs, mybins)
        myhist += htemp
        if i % 1000 == 0:
            logger.info("Done %s users out of %s.", i, samples)
    print(myhist)
    print(mybins)

    bax = brokenaxes(ylims=((0, 100000000), (1200000000, 1600000000)))
    bax.bar(mybins[:-1], myhist, width=0.04, color="orangered")
    plt.savefig("corrs_user_to_user.png")


def main():
    config = conf.get_config("prod")
    # You can uncomment the line below to use only small part of data to debug faster.
    #config["train_data_path"]["playing_data"] = "/data/train/areena_watching_times_2018-10-16_0010000_part_00.csv"
    _init_logging(logger, log_to_stdout=True)

    config["algorithm"] = "user_to_user"

    if config["algorithm"] == "item_to_item":
        item_to_item(config)
    elif config["algorithm"] == "user_to_user":
        user_to_user(config)
