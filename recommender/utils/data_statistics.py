import matplotlib.pyplot as plt
import logging
import sys
import recommender.datareader.prepare_data as prep
from recommender.config import config as conf


logger = logging.getLogger()


def _init_logging(logger, log_to_stdout = False):
    LOG_FORMAT = "%(asctime)s - %(levelname)s - %(name)s - %(message)s"

    log_filepath = 'mem-rec-data-statistics.log'
    file_handler = logging.FileHandler(log_filepath)
    formatter = logging.Formatter(LOG_FORMAT)
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.INFO)
    logger.addHandler(file_handler)
    logger.setLevel(logging.INFO)

    if log_to_stdout:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(logging.DEBUG)
        stdout_handler.setFormatter(formatter)
        logger.addHandler(stdout_handler)


def calc_user_types(df):
    df_desktop = df.loc[df.is_desktop == True]
    df_desktop = df_desktop.groupby('user_id')['series_mapped_id'].apply(list)
    df_mobile = df.loc[df.is_desktop == False]
    df_mobile = df_mobile.groupby('user_id')['series_mapped_id'].apply(list)
    return df_desktop.shape[0], df_mobile.shape[0]


def calc_stats(df):
    total_plays = 0
    play_categories = {}
    play_categories["1"] = 0
    play_categories["1 < x < 5"] = 0
    play_categories[">= 5"] = 0

    plays_by_users = {}
    plays_by_programs = {}

    max_played_programs_by_single_user = 0
    max_program_plays = 0
    max_user = ""

    for count, (user_id, watched_program_ids) in enumerate(df.iteritems(), 1):
        watched_program_ids = list(set(watched_program_ids)) # Remove duplicates with set
        amount_of_programs = len(watched_program_ids)
        total_plays += amount_of_programs

        if amount_of_programs == 1:
            play_categories["1"] += 1
        elif amount_of_programs > 1 and amount_of_programs < 5:
            play_categories["1 < x < 5"] += 1
        else:
            play_categories[">= 5"] += 1

        if amount_of_programs > max_played_programs_by_single_user:
            max_played_programs_by_single_user = amount_of_programs
            max_user = user_id

        if amount_of_programs in plays_by_users:
            plays_by_users[amount_of_programs] += 1
        else:
            plays_by_users[amount_of_programs] = 1

        for program in watched_program_ids:
            if program in plays_by_programs:
                plays_by_programs[program] += 1
            else:
                plays_by_programs[program] = 1
            if plays_by_programs[program] > max_program_plays:
                max_program_plays = plays_by_programs[program]

    print("Total amount of plays:", total_plays)
    print("Largest number of programs watched by single user id:", max_played_programs_by_single_user, ", user id:",
          max_user)

    return play_categories, \
           plays_by_programs, \
           plays_by_users, \
           max_played_programs_by_single_user, \
           max_program_plays


def program_stats(plays_by_programs):
    program_categories = {}
    program_categories["1"] = 0
    program_categories["1 < x < 10"] = 0  # Amount of programs which have been watched less than 10 times
    program_categories["10 <= x < 100"] = 0
    program_categories[">= 100"] = 0

    for program, plays in plays_by_programs.items():
        if plays == 1:
            program_categories["1"] += 1
        elif plays < 10 and plays > 1:
            program_categories["1 < x < 10"] += 1
        elif plays >= 10 and plays < 100:
            program_categories["10 <= x < 100"] += 1
        else:
            program_categories[">= 100"] += 1
    return program_categories


def main():
    config = conf.get_config("prod")
    # You can uncomment the line below to use only small part of data to debug faster.
    #config["train_data_path"]["playing_data"] = "/data/train/areena_watching_times_2018-10-16_0010000_part_00.csv"
    _init_logging(logger, log_to_stdout = True)
    df = prep.load_and_clean_playing_data(config)
    desktop_users, mobile_users = calc_user_types(df)
    df = df.groupby('user_id')['series_mapped_id'].apply(list)
    total_users = df.shape[0]

    print("Total amout of users:", total_users)
    print("Amount of desktop users:", desktop_users)
    print("Amount of mobile users:", mobile_users)

    play_categories, \
    plays_by_programs, \
    plays_by_users, \
    max_played_programs_by_single_user, \
    max_program_plays = calc_stats(df)

    total_programs = len(plays_by_programs)

    print("Total amount of programs:", total_programs)
    print("Amount of plays for most popular program:", max_program_plays)

    program_categories = program_stats(plays_by_programs)

    print("Amount of users who have played only one program:", play_categories["1"], "out of", total_users, "users.")
    print("Amount of programs which have only one viewer:", program_categories["1"], "out of", total_programs, "programs.")

    plays_by_users_sorted = dict(sorted(plays_by_users.items(), key=lambda kv: kv[1]))
    plays_by_programs_sorted = dict(sorted(plays_by_programs.items(), key=lambda kv: kv[1], reverse=True))

    fig = plt.figure()
    plt.bar(list(plays_by_users_sorted.keys()), plays_by_users_sorted.values(), color='g')
    cur_axes = plt.gca()
    cur_axes.set_xlim([1, max_played_programs_by_single_user + 50])
    fig.suptitle('Katsottujen sarjojen määrän jakautuminen')
    plt.xlabel('Katsottujen sarjojen lukumäärä käyttäjää kohden')
    plt.ylabel('Käyttäjien lukumäärä')
    plt.savefig('plays_by_users.png', bbox_inches='tight')

    fig = plt.figure()
    plt.bar(list(plays_by_users_sorted.keys()), plays_by_users_sorted.values(), color='g', log=True)
    cur_axes = plt.gca()
    cur_axes.set_xlim([1, max_played_programs_by_single_user + 50])
    fig.suptitle('Katsottujen sarjojen määrän jakautuminen')
    plt.xlabel('Katsottujen sarjojen lukumäärä käyttäjää kohden')
    plt.ylabel('Käyttäjien lukumäärä')
    plt.savefig('plays_by_users_log.png', bbox_inches='tight')

    fig = plt.figure()
    plt.bar(list(play_categories.keys()), play_categories.values(), color='g')
    fig.suptitle('Katsottujen sarjojen määrän jakautuminen')
    plt.xlabel('Katsottujen sarjojen lukumäärä käyttäjää kohden')
    plt.ylabel('Käyttäjien lukumäärä')
    plt.savefig('plays_by_users_cat.png', bbox_inches='tight')

    fig = plt.figure()
    plt.plot(list(plays_by_programs_sorted.keys()), plays_by_programs_sorted.values(), color='g')
    cur_axes = plt.gca()
    x_axis_values = [100, 800, 1616]
    x_axis_str = ["100", "800", "1616"]
    plt.xticks(x_axis_values, x_axis_str)
    cur_axes.set_ylim([0, max_program_plays + 10000])
    fig.suptitle('Katsomiskertojen jakautuminen sarjojen kesken')
    plt.xlabel('Sarjat')
    plt.ylabel('Katsomiskertojen lukumäärä')
    plt.savefig('plays_by_programs.png', bbox_inches='tight')

    fig = plt.figure()
    plt.bar(list(program_categories.keys()), program_categories.values(), color='g')
    fig.suptitle('Katsottujen ohjelmien määrän jakautuminen')
    plt.xlabel('Sarjan katsomiskertojen lukumäärä')
    plt.ylabel('Sarjojen lukumäärä')
    plt.savefig('plays_by_programs_cat.png', bbox_inches='tight')
