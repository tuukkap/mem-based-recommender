from recommender.datareader import import_data as imp, import_programs_api_data as progs
from recommender.datareader import matrix as mtx
import numpy as np
import logging


logger = logging.getLogger(__name__)


def create_datasets(config):
    df = load_and_clean_playing_data(config)
    popularity_list = _create_popularity_list(df)
    df = _combine_users_and_series(df)
    logger.info("Started creating data matrices.")
    rating_mat, played_mat = mtx.get_play_and_rating_mtxs(df)
    avg_rating_mtx = mtx.get_avg_rating_mtx(rating_mat)
    logger.info("Finished creating data matrices.")
    logger.info("Rating matrix shape: %s.", rating_mat.shape)
    return df, rating_mat, played_mat, avg_rating_mtx, popularity_list


def load_and_clean_playing_data(config):
    metadata_df = progs.import_programs_api_data(config)
    df = imp.load_data(config)
    df = _clean_data(config, df, metadata_df)
    logger.info("Computing of dask dataframe started.")
    df = df.compute()
    logger.info("Computing of dask dataframe finished.")
    logger.debug("Dataframe shape: %s", df.shape)
    return df


def _combine_users_and_series(df):
    df.set_index(['user_id', 'series_mapped_id'], inplace=True)
    logger.info("Started to group data by series id.")
    df = df.groupby(['user_id', 'series_mapped_id'])['percentage_played'].mean()
    logger.info("Finished grouping data by series id.")
    return df


def _create_popularity_list(df, K = 100):
    df = df[['series_mapped_id', 'played']].groupby('series_mapped_id').sum()
    df = df.sort_values(by=['played'], ascending=False)
    popularity_list = df.index.tolist()[:K]
    return popularity_list


def _clean_data(config, df, metadata_df):
    '''
    Returns a cleaned df with indexes: user_id and series_mapped_id
    and columns: user_id (index), series_mapped_id (index), program_id, is_desktop, percentage_played, played
    '''
    logger.info("Cleaning up data started.")
    df = _reduce_to_childrens_programs(df, metadata_df)
    df = _reduce_to_videos(df, metadata_df)
    df = _add_series_id(df, metadata_df)

    # Clean empty strings and rows without necessary data values.
    df = df.map_partitions(lambda x: x.replace('', np.nan))
    df = df.dropna(subset=['ns_st_pt', 'avg_yle_truelength', 'program_id'])

    # combine IDs to one column and add another column for user type (desktop = 0 or mobile = 1)
    df = df.assign(user_id = df.yle_selva.combine_first(df.c12), is_desktop = df.yle_selva.notnull())

    # Calculate percentage played for train and test (this has to be done before compute()).
    df = df.assign(percentage_played = (df['ns_st_pt'] / df['avg_yle_truelength']))

    # Helps to create sparse matrices
    df = df.assign(played = 1)

    # Replace over 100 % played values with 100 % as individual play values shouldn't be over 100 %.
    df.percentage_played = df.percentage_played.mask(df['percentage_played'] > 1, 1)

    # Drop unnecessary columns.
    df = df.drop(['yle_selva', 'c12', 'ns_st_pt', 'avg_yle_truelength', 'min_collectorreceived'], axis='columns')
    df = df.map_partitions(lambda x: x.replace(b'', np.nan))
    df = df.dropna(subset = ['user_id'])
    df = df.drop_duplicates(subset = ['user_id', 'program_id'])
    df = df.loc[df.percentage_played > config['minimum_play_percentage']]
    logger.info("Cleaning up data finished.")
    return df


def _reduce_to_childrens_programs(df, metadata_df):
    is_children_program = metadata_df.set_index('item_id')['categories'].str.contains("5-195", na=0)
    correct_set = set(x for x in metadata_df.item_id if is_children_program[x])
    return df[df.program_id.isin(correct_set)]


def _reduce_to_videos(df, metadata_df):
    content_type = metadata_df.set_index('item_id')['typeMedia']
    correct_set = set(x for x in metadata_df.item_id if content_type[x] == 'TVContent')
    return df[df.program_id.isin(correct_set)]


def _add_series_id(df, metadata_df):
    '''
    This function is originated from Areena recommender.
    '''
    item_to_series = _item_to_series(metadata_df)
    return df.assign(series_mapped_id = df.program_id.map(lambda pid: item_to_series.get(pid) or pid))


def _item_to_series(metadata_df):
    """
    This function is originated from Areena recommender.

    Convert program metadata into a mapping from single episodes to series.
    Note that this implicitly assumes the metadata contains program info for at least data_period_days days (as defined
    in the config object).
    :param metadata_df: a Dask dataframe of metadata, as returned by read_metadata
    :return: a dict with the program id -> series id mapping containing only program ids that should be hard
      aggregated together. Hard aggregation is always done, and is implemented in the backend (``support/``). Soft
      aggregation is done only sometimes, and is implemented in the frontend (``api/``).
    """
    def filter_program_ids_by_aggregatable_series(df):
        aggregatable_series_ids = df[df.is_aggregatable.astype(str) == 'True'].item_id.values
        return df[df.series_id.isin(aggregatable_series_ids)]

    def filter_unaggregatable_program_ids(df):
        "Filter out program ids that should not be hard aggregated even if they are part of an aggregatable series."
        return df[~df['type'].isin(['TVClip', 'RadioClip'])]

    def convert_to_series_id_by_item_id_dict(df):
        return df.set_index('item_id').series_id.to_dict()

    item_to_series = (metadata_df.pipe(filter_program_ids_by_aggregatable_series)
                                 .pipe(filter_unaggregatable_program_ids)
                                 .pipe(convert_to_series_id_by_item_id_dict))
    return item_to_series


def get_id_dicts(df):
    levels = {0: "user", 1: "prog"}
    dicts = {}
    # Form a dict from both indexes (user and program) in the dataframe.
    for level in range(2):
        ids = df.index.get_level_values(level)
        id_numbers = df.index.labels[level]
        zipobj = zip(id_numbers, ids)
        num_to_id = dict(zipobj)
        zipobj2 = zip(ids, id_numbers)
        id_to_num = dict(zipobj2)
        dicts[str(levels[level]) + "_num_to_id"] = num_to_id
        dicts[str(levels[level]) + "_id_to_num"] = id_to_num
    return dicts
