import udatetime
import pytz
import os
import pandas as pd
import numpy as np
import logging

logger = logging.getLogger(__name__)


def import_programs_api_data(config):
    """
    This function is inspired by similar function in Areena recommender.
    """
    def parse_date(date):
        if isinstance(date, float) and np.isnan(date):
            return np.nan
        else:
            try:
                return udatetime.from_string(date).astimezone(pytz.UTC).replace(tzinfo=None)
            except ValueError:
                return np.nan

    THIS_DIR = os.path.dirname(os.path.abspath(__file__))
    path = THIS_DIR + "/../.." + config["train_data_path"]["programs_api_data"]

    header_names = ['item_id', 'series_id', 'type', 'typeMedia', 'categories', 'availability',
                    'start_time', 'end_time', 'title', 'image_id', 'episode', 'is_aggregatable',
                    'duration', 'publishers', 'audio_languages', 'subtitle_languages',
                    'title_languages', 'description_languages']
    dtypes = {'is_aggregatable': str, 'duration': float}
    logger.info("Loading metadata from %s.", path)
    df = pd.read_csv(path, sep='\t', header=None, names=header_names, dtype=dtypes,
                     parse_dates=['start_time', 'end_time'],
                     date_parser=parse_date)
    logger.info("Metadata loaded.")
    return df
