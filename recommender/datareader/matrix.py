import scipy.sparse as sps
import numpy as np


def get_play_and_rating_mtxs(df):
    rating_mat = _create_sparse_matrix(df)
    played_mat = _create_sparse_played_matrix(df)
    return rating_mat, played_mat


def get_avg_rating_mtx(rating_mat, axis = 0):
    '''
    Average rating matrix is a matrix where there's a row for each user and user's ratings are replaced
    by user's average ratings. This is needed when calculating user correlations.

    :param rating_mat:
    :param df: dask dataframe consisting of user data
    :return: average rating matrix
    '''
    avg_ratings = _calc_avg_ratings(rating_mat, axis)
    (rows, cols, values) = sps.find(rating_mat)
    if axis == 1:
        indexes = cols
    else:
        indexes = rows
    data = []
    for index in indexes:
        data.append(avg_ratings[index])
    mtx = sps.csr_matrix((data, (rows, cols)))
    return mtx


def _create_sparse_matrix(df):
    mtx = sps.csr_matrix((df.values, (df.index.labels[0], df.index.labels[1])))
    return mtx


def _create_sparse_played_matrix(df):
    '''Create matrix which contains 1 at those user rows and program columns
    which the user has played (i.e. watched or listened).'''
    played_bool = df.values > 0
    played = played_bool.astype(int)
    mtx = sps.csr_matrix((played, (df.index.labels[0], df.index.labels[1])))
    return mtx


def _calc_avg_ratings(rating_mat, axis = 0):
    '''
    Calculates average rating from either rows (users) or columns (items).
    '''
    (rows, cols, values) = sps.find(rating_mat)
    if axis == 1:
        axis_to_count = cols
    else:
        axis_to_count = rows
    amount_of_values = np.bincount(axis_to_count)
    sum_of_values = np.bincount(axis_to_count, weights = values)
    averages = np.divide(sum_of_values, amount_of_values, out = np.zeros_like(sum_of_values), where = amount_of_values != 0)
    return averages
