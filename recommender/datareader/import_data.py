import dask.dataframe as dd
import numpy as np
import re
import logging
import os


logger = logging.getLogger(__name__)


def load_data(config):
    '''
    This function is inspired by similar function in Areena recommender.
    '''
    string_type = 'a64'
    header = ['yle_selva', 'c12', 'program_id', 'ns_st_pt', 'avg_yle_truelength', 'min_collectorreceived']
    dtypes = {'c12': string_type, 'ns_st_pt': np.float32, 'avg_yle_truelength': np.float32}

    def remove_special_chars(x):
        return re.sub(r'[^\x00-\x7F]+', '', x)

    THIS_DIR = os.path.dirname(os.path.abspath(__file__))
    path = THIS_DIR + "/../.." + config["train_data_path"]["playing_data"]

    logger.info("Loading data from %s.", path)
    df = dd.read_csv(path,
                     names=header, sep='|',
                     converters={'yle_selva': remove_special_chars, 'program_id': remove_special_chars},
                     dtype=dtypes,
                     encoding='utf-8',
                     blocksize=None)
    logger.info("Data loaded.")
    return df
