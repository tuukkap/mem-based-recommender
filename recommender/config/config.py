import json
import os


THIS_DIR = os.path.dirname(os.path.abspath(__file__))


def get_config(env):
    assert env in {"prod", "unittest"}
    if env == "prod":
        config_path = THIS_DIR + "/../../config/config_prod.json"
    elif env == "unittest":
        config_path = THIS_DIR + "/../../config/config_unittest.json"
    with open(config_path) as f:
        data = json.load(f)
    return data