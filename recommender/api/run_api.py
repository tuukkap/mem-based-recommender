from flask import Flask, jsonify
import sys
from flask_restful import Resource, Api, reqparse
import logging
import random
import argparse
from recommender.recommendation_algorithms import memory_based_algorithms as algo
from recommender.datareader import prepare_data as prep
from recommender.config import config as conf
from recommender.recommendation_algorithms import user_recommendations as recs


logger = logging.getLogger()


def _init_logging(logger, app, log_to_stdout = False):
    LOG_FORMAT = "%(asctime)s - %(levelname)s - %(name)s - %(message)s"

    log_filepath = 'mem-rec-flask.log'
    handler = logging.FileHandler(log_filepath)
    formatter = logging.Formatter(LOG_FORMAT)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    app.logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    werkzeug_logger = logging.getLogger('werkzeug')
    werkzeug_logger.setLevel(logging.ERROR) # At info level werkzeug will log every request

    if log_to_stdout:
        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(logging.DEBUG)
        stdout_handler.setFormatter(formatter)
        logger.addHandler(stdout_handler)
    return handler


def _build_response(item_list):
    return [{'id': item_id} for item_id in item_list]


def get_recommendations(user_id, user_type, limit, recommender_type):
    assert user_type in ("desktop", "mobile")
    assert recommender_type in ("memory-based", "random", "popularity")

    if user_type == "mobile":
        user_id = user_id.encode()

    use_random_recommender = (user_id not in user_id_to_num or recommender_type == "random")\
                             and recommender_type != "popularity"
    if recommender_type == "popularity":
        predictions_list = popularity_list[:limit]
    elif use_random_recommender:
        predictions_list = random.sample(range(rating_mat.shape[1]), rating_mat.shape[1])
        predictions_list = [prog_num_to_id[item] for item in predictions_list]
        predictions_list = predictions_list[:limit]
    else:
        user_num = user_id_to_num[user_id]
        if config["algorithm"] == "item_to_item":
            predictions = algo.item_to_item_prediction(config, user_num, rating_mat, played_mat, avg_rating_mtx, corr_cache)
        elif config["algorithm"] == "user_to_user_1":
            predictions = algo.user_to_user_prediction_1(config, user_num, rating_mat, played_mat, avg_rating_mtx)
        elif config["algorithm"] == "user_to_user_2_abs":
            predictions = algo.user_to_user_prediction_2(config, user_num, rating_mat, played_mat, avg_rating_mtx,
                                                         use_abs_denominator=True)
        elif config["algorithm"] == "user_to_user_2":
            predictions = algo.user_to_user_prediction_2(config, user_num, rating_mat, played_mat, avg_rating_mtx,
                                                         use_abs_denominator=False)
        predictions_list = recs.get_user_recommendations_list(predictions, prog_num_to_id, played_mat, user_num, limit)
    predictions_json = _build_response(predictions_list)

    return jsonify(data = predictions_json, random = use_random_recommender)


class Recommendations(Resource):
    def get(self):
        args = parser.parse_args()
        preds = get_recommendations(args['user_id'], args['user_type'], args['limit'], args['recommender_type'])
        return preds


def _parse_script_args():
    '''
    These are arguments to be used when starting the API to determine what kind of source data
    and what kind of algorithm will be used with the recommendation engine.
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--use-sample-data', dest='use_sample_data', action='store_true')
    parser.add_argument('--algorithm',
                        choices = ['user_to_user_1', 'user_to_user_2_abs', 'user_to_user_2', 'item_to_item'],
                        default='user_to_user_2')
    parser.set_defaults(use_sample_data=False)
    return parser.parse_args()


def _parse_request_args(parser):
    '''
    These are arguments to be used with urls when requesting recommendations.
    '''
    parser.add_argument('limit', type=int, help="The amount of recommendations for user.", required=True)
    parser.add_argument("user_id", type=str, help="User ID.", required=True)
    parser.add_argument("user_type", type=str, choices=("desktop", "mobile"), help="User type", default="desktop",
                        required=True)
    parser.add_argument("recommender_type", choices=("memory-based", "random", "popularity"),
                        help="Recommendation method.", default="memory-based")


def main():
    global flask_app, flask_api, config, df, rating_mat, played_mat,avg_rating_mtx, user_id_to_num, prog_num_to_id
    global parser, corr_cache, popularity_list

    flask_app = Flask(__name__)
    flask_api = Api(flask_app)

    args = _parse_script_args()

    _init_logging(logger, flask_app)

    parser = reqparse.RequestParser()
    _parse_request_args(parser)

    config = conf.get_config("prod")
    config["algorithm"] = args.algorithm
    logger.info("Selected algorithm is %s.", config["algorithm"])

    if config["algorithm"] == "item_to_item":
        corr_cache = {}

    if args.use_sample_data:
        config["train_data_path"]["playing_data"] = "/data/train/areena_watching_times_2018-10-16_0010000_part_00.csv"

    df, rating_mat, played_mat, avg_rating_mtx, popularity_list = prep.create_datasets(config)

    dicts = prep.get_id_dicts(df)
    user_id_to_num = dicts['user_id_to_num']
    prog_num_to_id = dicts['prog_num_to_id']

    flask_api.add_resource(Recommendations, '/')
    logger.info("Flask server should be now running.")
    flask_app.run(debug=False)
