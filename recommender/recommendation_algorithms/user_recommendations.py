import pandas as pd
import numpy as np


def get_user_recommendations_list(predictions, prog_num_to_id, played_mat, user_num, limit, filter_played_programs = True):
    df_results = pd.DataFrame(predictions.T)
    df_results['program_id'] = df_results.index

    if filter_played_programs:
        user_played_progs = played_mat[user_num,].todense()
        user_played_progs = np.squeeze(np.asarray(user_played_progs))
        df_results["played_previously"] = user_played_progs
        df_results = df_results[df_results["played_previously"] == 0]
        df_results = df_results.drop(['played_previously'], axis='columns')

    df_results = df_results.sort_values(by=[0], ascending=False)
    df_results = df_results[:limit]
    prog_recs_list = df_results["program_id"].values.tolist()
    prog_recs_list = [prog_num_to_id[item] for item in prog_recs_list]
    return prog_recs_list
