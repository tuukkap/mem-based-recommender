import numpy as np
import logging


logger = logging.getLogger(__name__)


def user_to_user_prediction_1(config, user, rating_mat, played_mat, avg_rating_mtx):
    '''
    This algorithm follows formula found at "Empirical Analysis of Predictive Algorithms for Collaborative Filtering"
    at second page where predicted vote p(a,j) for user u for program i is:

    P(u,i) = Y_avg(a) + K (sum(i' = 1 to n) W(u,u') * (Y(u',i) - Y_avg(u')))

    where Y_avg(a) is average vote (rating) for all programs user a has played, K is normalizing factor, W(u,u') is
    correlation value between users u and u' and Y(u',i) is vote of user u' for program i.

    '''
    def get_avg_rating(avg_rating_mtx, user):
        if avg_rating_mtx[user, :].count_nonzero() == 0:
            return 0
        nonzero_rows, nonzero_columns = avg_rating_mtx[user, :].nonzero()
        avg_rating = avg_rating_mtx[user, nonzero_columns[0]]
        return avg_rating

    user_corrs = get_correlations(config, user, rating_mat, played_mat, avg_rating_mtx)
    user_corrs[0, user] = 0
    sum_of_abs = np.sum(np.absolute(user_corrs))
    if sum_of_abs > 0:
        K = 1 / sum_of_abs
    else:
        K = 0
    result_suffix = user_corrs @ (rating_mat - avg_rating_mtx)
    avg_rating = get_avg_rating(avg_rating_mtx, user)
    result = avg_rating + K * result_suffix
    return result


def user_to_user_prediction_2(config, user, rating_mat, played_mat, avg_rating_mtx, use_abs_denominator = True):
    '''
    The formula used here is a modification from the paper "Collaborative Filtering for Implicit Feedback Datasets"
    by Yifan Hu et al, where a similar formula was used with item to item recommendation. Here the formula is modified
    to calculate user to user recommendation.

    The modified formula is following:

    P_u,i = ((sum over each user u') W_u,u' * Y_u,i) / ((sum over each user u') | W_u,u' |)

    where P_u,i is  the estimated rating for user about program i, W_u,u' is Pearson correlation between
    users u and u' (their similarity) and Y_u,i is rating which user u has given for program i.

    Below this is calculated with matrices in batches of several users and all programs at a time:

    P_U = (W_U @ Y_U.T) /
          sum_of_rows(| W_U |)

    where P_U is predicted rating for user batch U from all programs. W_U is Pearson correlation (similarity)
    between user batch U and all other users. Thus W_U is a matrix of size (number of users in batch U) x
    (number of all users). Y_U means ratings by users in batch U from all programs.

    '''
    user_corrs = get_correlations(config, user, rating_mat, played_mat, avg_rating_mtx)
    user_corrs[0, user] = 0
    numerator = user_corrs @ rating_mat
    if use_abs_denominator:
        denominator = abs(user_corrs).sum(axis = 1)
    else:
        denominator = user_corrs.sum(axis = 1)
    result = np.divide(numerator, denominator, out=np.zeros_like(numerator), where=denominator != 0)
    return result


def item_to_item_prediction(config, user, rating_mat, played_mat, avg_rating_matrix, corr_cache):
    '''
    The formula used here is from the paper "Collaborative Filtering for Implicit Feedback Datasets"
    by Yifan Hu et al. The estimated rating from user u about item i is r(estimated)_ui:

    P_u,i = ((sum over each item i') W_i,i' * Y_u,i) / ((sum over each item i') | W_i,i' |)

    where W_i,i' is Pearson correlation between items i and i' (their similarity) and Y_u,i is rating
    which user u has given for program i.

    Below this is calculated with matrices in batches of several users and all programs at a time:

    P_U = (W_U @ Y_U) /
          sum_of_rows(W_U)

    where P_U is predicted rating for user batch U from all programs. W_U is Pearson correlation (similarity)
    between user batch U and all other users. Thus W_U is a matrix of size (number of users in batch U) x
    (number of all users). Y_U means ratings by users in batch U from all programs.

    '''
    progs_watched_by_user = played_mat[user,].nonzero()[1]
    item_corrs = np.zeros((rating_mat.shape[1], rating_mat.shape[1]))
    for prog in progs_watched_by_user:
        if prog in corr_cache:
            item_corr_row = corr_cache[prog]
        else:
            item_corr_row = get_correlations(config, prog, rating_mat, played_mat, avg_rating_matrix)
            corr_cache[prog] = item_corr_row
        item_corr_row[0, prog] = 0
        item_corrs[:,prog] = item_corr_row
    rating_mat_trans = rating_mat[user, ].T
    num = (item_corrs @ rating_mat_trans).reshape((rating_mat.shape[1],))
    den = abs(item_corrs).sum(axis = 1)
    result = np.divide(num, den, out=np.zeros_like(num), where=den != 0)
    return result


def get_correlations(config, user_or_program, rating_mat, played_mat, avg_rating_mtx):
    '''
    This method follows formula found at "Empirical Analysis of Predictive Algorithms for Collaborative Filtering"
    at 2.1.1 where correlation w(a,i) between users a and i is:

    w(a,i) = ( sum(j = 1 to n) ( (v(a,j) - v_avg(a)) * (v(i,j) - v_avg(i)) ) ) /
             sqrt( ( sum(j = 1 to n) ( (v(a,j) - v_avg(a))**2 ) * ( sum(j = 1 to n) ( (v(i,j) - v_avg(i))**2 ) )

    where v_avg(x) is average rating for all programs user x has played and v(i,j) is rating of user i for program j.

    Below this is calculated for all correlations for one user using matrices and vectors. The same logic above
    concerns also correlation for item based recommendation.

    :param config: configuration json
    :param range: range of either users or items (depends on the selected algorithm at config)
    :param rating_mat: matrix of implicit user ratings (actual playing data)
    :param avg_rating_mtx: matrix of users' average ratings placed at the same rows and cols users have playing data
    :return: correlation vector (or 1 row matrix) between 'user_num' and all other users
    '''
    numerators = (rating_mat - avg_rating_mtx)
    if config["algorithm"] == "item_to_item":
        numerator1 = numerators.T
        numerator2 = numerators
        played_mat = played_mat.T
    else:
        numerator1 = numerators
        numerator2 = numerators.T
    numerator1 = numerator1[user_or_program, ]
    numerator = numerator1 @ numerator2
    denominator1 = numerator1.power(2)
    denominator2 = numerator2.power(2)
    # The matrices are transposed above so that the vectors to be correlated (and summed) are always
    # rows with num1 and den1, and cols with num2 and den2.
    # Denominators should contain only items which are common to user or program.
    # i.e.: if user to user, then both denominators should contain programs which user at den1 and den2 have played.
    # if item to item, then denominators should contain users which both have seen the program in question.
    denominator1 = played_mat.multiply(denominator1)
    denominator1 = denominator1.sum(axis = 1)
    denominator2 = denominator2.multiply(played_mat[user_or_program].T)
    denominator2 = denominator2.sum(axis = 0).T
    denominator = np.sqrt(np.multiply(denominator1, denominator2))
    numerator = numerator.todense()
    denominator = denominator.T
    # Sometimes denominator goes to zero. At those times put zero to result.
    result = np.divide(numerator, denominator, out=np.zeros_like(numerator), where=denominator != 0)
    return result
