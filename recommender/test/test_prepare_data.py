import unittest
import scipy.sparse as sps
import pandas.util.testing as pdtest
from recommender.datareader import import_data as imp, prepare_data as prep, import_programs_api_data as progs
from recommender.datareader import matrix as mtx
from recommender.config import config as conf
import numpy as np
import pandas as pd
import os


THIS_DIR = os.path.dirname(os.path.abspath(__file__))


class testPrepare(unittest.TestCase):

    """
    Test csv contents below

    - Rows marked with star will be removed. The reason to remove is written after the star

    yle_selva_1||prog_of_series_2|50|50|1
    yle_selva_1||prog_1|30|50|6
    yle_selva_1||prog_of_series_3|45|50|11
    |c12_1|prog_of_series_2|40|50|2
    |c12_1|prog_1|17|50|7 * played under 40 %
    |c12_1|prog_2|45|50|12
    yle_selva_2||prog_of_series_2|37|50|3
    yle_selva_2||prog_2|26|50|8
    |c12_2|prog_of_series_1|25|50|4
    |c12_2|prog_1|23|50|9
    |c12_2|prog_of_series_3|41|50|13
    yle_selva_3||prog_of_series_2|34|50|5
    yle_selva_3||prog_1|23|50|10
    ||prog_1|32|50|432 * both user id's missing
    yle_selva_4||prog_4||50|4 * played duration missing
    yle_selva_5||prog_4|34||5 * total duration missing
    yle_selva_6|||45|50|6 * program id missing
    yle_selva_1||prog_3|45|50|2 * Content is audio
    |c12_1|prog_of_series_4|45|50|2 * Isn't children's program
    """
    # Resulting columns user_id (index), series_mapped_id (index), program_id, percentage_played, played
    data = [['yle_selva_1', 'series_2', 'prog_of_series_2', True, 1, 1],
            ['yle_selva_1', 'prog_1', 'prog_1', True, 0.6, 1],
            ['yle_selva_1', 'series_3', 'prog_of_series_3', True, 0.9, 1],
            [b'c12_1', 'series_2', 'prog_of_series_2', False, 0.8, 1],
            [b'c12_1', 'prog_2', 'prog_2', False, 0.9, 1],
            ['yle_selva_2', 'series_2', 'prog_of_series_2', True, 0.74, 1],
            ['yle_selva_2', 'prog_2', 'prog_2', True, 0.52, 1],
            [b'c12_2', 'series_1', 'prog_of_series_1', False, 0.5, 1],
            [b'c12_2', 'prog_1', 'prog_1', False, 0.46, 1],
            [b'c12_2', 'series_3', 'prog_of_series_3', False, 0.82, 1],
            ['yle_selva_3', 'series_2', 'prog_of_series_2', True, 0.68, 1],
            ['yle_selva_3', 'prog_1', 'prog_1', True, 0.64, 1]]
    columns = ['user_id', 'series_mapped_id', 'program_id', 'is_desktop', 'percentage_played', 'played']
    expected_df_after_clean = pd.DataFrame(data, columns = columns)
    expected_df_after_clean.set_index(['user_id', 'series_mapped_id'], inplace=True)
    expected_df_after_clean_dict = expected_df_after_clean.to_dict()


    def setUp(self):
        self.config = conf.get_config("unittest")
        self.ungrouped_df = prep.load_and_clean_playing_data(self.config)
        self.df = prep._combine_users_and_series(self.ungrouped_df)


    def test_clean_data(self):
        try:
            pdtest.assert_frame_equal(self.ungrouped_df, self.expected_df_after_clean)
        except Exception:
            self.fail("test_clean_data raised Exception")


    def test_create_user_and_prog_id_dict_key_to_num(self):
        try:
            mat = mtx._create_sparse_matrix(self.df)
            dicts = prep.get_id_dicts(self.df)
            user_id_to_num = dicts['user_id_to_num']
            prog_id_to_num = dicts['prog_id_to_num']
            success = True
            for (user_id, prog_id), value in self.df.iteritems():
                expected_result = value
                user_num = user_id_to_num[user_id]
                prog_num = prog_id_to_num[prog_id]
                result = mat[user_num, prog_num]
                if expected_result == result:
                    continue
                else:
                    success = False
                    break
            self.assertTrue(success)
        except Exception:
            self.fail("test_create_user_id_dict raised Exception")


    def test_create_user_and_prog_id_dict_num_to_key(self):
        try:
            mat = mtx._create_sparse_matrix(self.df)
            dicts = prep.get_id_dicts(self.df)
            user_num_to_id = dicts['user_num_to_id']
            prog_num_to_id = dicts['prog_num_to_id']
            success = True
            for row in range(mat.shape[0]):
                for col in range(mat.shape[1]):
                    user_id = user_num_to_id[row]
                    prog_id = prog_num_to_id[col]
                    expected_result = mat[row, col]
                    # If value was found...
                    if expected_result > 0:
                        result = self.df.loc[user_id, prog_id]
                        # ... is it correct value?
                        if expected_result == result:
                            continue
                        else:
                            success = False
                            break
                    # If value wasn't found...
                    else:
                        # ... then it shouldn't be found at df
                        if prog_id in self.df.loc[user_id].index.values:
                            success = False
                            break
            self.assertTrue(success)
        except Exception:
            self.fail("test_create_user_id_dict raised Exception")


    def test_create_sparse_matrix(self):
        mat = mtx._create_sparse_matrix(self.df)
        dicts = prep.get_id_dicts(self.df)
        user_num_to_id = dicts['user_num_to_id']
        user_id_to_num = dicts['user_id_to_num']
        prog_num_to_id = dicts['prog_num_to_id']
        prog_id_to_num = dicts['prog_id_to_num']
        success = True
        # Check that matrix contains all the values in the df
        for (user_id, series_id), play_percentage in self.df.iteritems():
            user_num = user_id_to_num[user_id]
            series_num = prog_id_to_num[series_id]
            expected_result = play_percentage
            result = mat[user_num, series_num]
            if result == expected_result:
                continue
            else:
                success = False
                break
        # Check that matrix doesn't contain anything else than in the df
        for user in range(mat.shape[0]):
            for series in range(mat.shape[1]):
                user_id = user_num_to_id[user]
                series_id = prog_num_to_id[series]
                if mat[user, series] > 0 and (not self.df.index.isin([(user_id, series_id)]).any()):
                    success = False
                    break
        self.assertTrue(success)


    def test_create_sparse_played_mtx(self):
        mat_train = mtx._create_sparse_matrix(self.df)
        played_train = mtx._create_sparse_played_matrix(self.df)
        mat_train_nonzero = mat_train.nonzero()
        played_train_nonzero = played_train.nonzero()
        train_nonzeros_ok = np.array_equal(mat_train_nonzero, played_train_nonzero)
        (rows, cols, values) = sps.find(played_train)
        values_ok = True
        for value in values:
            if value != 1:
                values_ok = False
                break
        self.assertTrue(train_nonzeros_ok and values_ok)


    def test_calc_avg_ratings(self):
        mat_train = mtx._create_sparse_matrix(self.df)
        dicts = prep.get_id_dicts(self.df)
        user_num_to_id = dicts['user_num_to_id']
        user_id_to_num = dicts['user_id_to_num']
        prog_num_to_id = dicts['prog_num_to_id']
        prog_id_to_num = dicts['prog_id_to_num']
        success = True
        df = self.df.groupby(['user_id']).mean()
        for user_id, mean in df.iteritems():
            user_num = user_id_to_num[user_id]
            expected_result = mean
            result = mtx._calc_avg_ratings(mat_train[user_num])[0]
            if expected_result == result:
                continue
            else:
                success = False
                break
        self.assertTrue(success)


    def test_get_avg_rating_mtx(self):
        mat_train = mtx._create_sparse_matrix(self.df)
        mat_avg_train = mtx.get_avg_rating_mtx(mat_train)
        mat_train_nonzero = mat_train.nonzero()
        mat_avg_train_nonzero = mat_avg_train.nonzero()
        self.assertTrue(np.array_equal(mat_train_nonzero, mat_avg_train_nonzero))
