import unittest
from recommender.recommendation_algorithms import memory_based_algorithms as recs
from recommender.datareader import matrix as mtx
from recommender.config import config as conf
import scipy.sparse as sps
import numpy as np
import math
import numpy.testing as nptest
from random import random


def _create_random_mtx(rows, cols, randseed = 42):
    np.random.seed(randseed)
    mat = np.random.rand(rows, cols)

    # Replace majority of data points with zeros as this should imitate sparse matrix.
    amount_of_zeros = int(round((rows * cols) * 0.999))
    rowindexes = np.random.choice(rows, amount_of_zeros)
    colindexes = np.random.choice(cols, amount_of_zeros)
    indexes = zip(rowindexes, colindexes)
    for row, col in indexes:
        mat[row, col] = 0
        if (mat != 0)[row].sum() == 0:
            mat[row, col] = random()
    return mat


class testPrepare(unittest.TestCase):


    def setUp(self):
        self.config = conf.get_config("unittest")
        self.no_of_users = 100
        self.no_of_items = 6
        self.rating_mat = _create_random_mtx(self.no_of_users, self.no_of_items, 42)
        self.rating_mat = sps.csr_matrix(self.rating_mat)
        self.avg_rating_mat = mtx.get_avg_rating_mtx(self.rating_mat)
        self.played_mat = self.rating_mat.copy()
        self.played_mat[self.played_mat > 0] = 1
        debug = False
        if debug:
            print("Rating matrix")
            print(self.rating_mat.todense())
            print("Average matrix")
            print(self.avg_rating_mat.todense())


    def test_user_to_user_prediction_1(self):
        self.config["algorithm"] = "user_to_user_1"

        def user_to_user_prediction_naive(user, item, rating_mat, played_mat, avg_rating_mat):

            def get_avg_rating(avg_rating_mtx, user):
                if avg_rating_mtx[user, :].count_nonzero() == 0:
                    return 0
                nonzero_rows, nonzero_columns = avg_rating_mtx[user,:].nonzero()
                avg_rating = avg_rating_mtx[user, nonzero_columns[0]]
                return avg_rating

            user_corrs = recs.get_correlations(self.config, user, rating_mat, played_mat, avg_rating_mat)
            K = 0
            result_suffix = 0
            user_corrs[0, user] = 0
            for corr in range(user_corrs.shape[1]):
                K += abs(user_corrs[0, corr])
            if (K != 0):
                K = 1 / K
            for u_prime in range(user_corrs.shape[1]):
                if user == u_prime:
                    continue
                else:
                    result_suffix += user_corrs[0, u_prime] * (rating_mat[u_prime, item] - avg_rating_mat[u_prime, item])
            avg_rating = get_avg_rating(avg_rating_mat, user)
            result = avg_rating + K * result_suffix
            return result

        user_recs_naive = np.zeros((self.no_of_users, self.no_of_items))
        for user in range(self.no_of_users):
            for item in range(self.no_of_items):
                user_recs_naive[user, item] = user_to_user_prediction_naive(user, item, self.rating_mat, self.played_mat, self.avg_rating_mat)
        user_recs_naive = np.nan_to_num(user_recs_naive)
        user_recs_result = np.zeros((self.no_of_users, self.no_of_items))
        for user in range(self.no_of_users):
            user_recs_result[user,] = recs.user_to_user_prediction_1(self.config, user, self.rating_mat, self.played_mat, self.avg_rating_mat)
        nptest.assert_array_almost_equal_nulp(user_recs_naive, user_recs_result, 187)

    def test_user_to_user_prediction_2(self):
        self.config["algorithm"] = "user_to_user_2"

        def user_to_user_prediction_naive(user, item, rating_mat, played_mat, avg_rating_mat):
            '''
            P_u,i = ((sum over each user u') W_u,u' * Y_u,i) / ((sum over each user u') W_u,u')

            Predict rating for user u of program i. u' are the other users and Y_u',i are their ratings.
            '''
            nom = 0
            den = 0
            user_corrs = recs.get_correlations(self.config, user, rating_mat, played_mat, avg_rating_mat)
            for u_prime in range(user_corrs.shape[1]):
                if user == u_prime:
                    continue
                else:
                    nom += user_corrs[:, u_prime] * rating_mat[u_prime, item]
                    den += abs(user_corrs[:, u_prime])
            if den == 0:
                return 0
            else:
                return nom / den

        user_recs_naive = np.zeros((self.no_of_users, self.no_of_items))
        for user in range(self.no_of_users):
            for item in range(self.no_of_items):
                user_recs_naive[user, item] = user_to_user_prediction_naive(user, item, self.rating_mat,
                                                                            self.played_mat, self.avg_rating_mat)
        user_recs_naive = np.nan_to_num(user_recs_naive)

        user_recs_result = np.zeros((self.no_of_users, self.no_of_items))
        for user in range(self.no_of_users):
            user_recs_result[user,] = recs.user_to_user_prediction_2(self.config, user, self.rating_mat, self.played_mat,
                                                                   self.avg_rating_mat)
        nptest.assert_array_almost_equal_nulp(user_recs_naive, user_recs_result, 187)

    def test_item_to_item_prediction(self):
        self.config["algorithm"] = "item_to_item"

        def item_to_item_prediction_naive(user, item, rating_mat, played_mat, avg_rating_mat):
            '''
            P_u,i = ((sum over each item i') W_i,i' * Y_u,i) / ((sum over each item i') W_i,i')

            Predict rating for user u of program i. u' are the other users and Y_u',i are their ratings.
            '''
            nom = 0
            den = 0
            item_corrs = recs.get_correlations(self.config, item, rating_mat, played_mat, avg_rating_mat)
            progs_watched_by_user = played_mat[user,].nonzero()[1]
            for i_prime in progs_watched_by_user:
                if i_prime == item:
                    continue
                else:
                    nom += item_corrs[0, i_prime] * rating_mat[user, i_prime]
                    den += abs(item_corrs[0, i_prime])
            if den == 0:
                return 0
            else:
                return nom / den

        user_recs_naive = np.zeros((self.no_of_users, self.no_of_items))
        for user in range(self.no_of_users):
            for item in range(self.no_of_items):
                user_recs_naive[user, item] = item_to_item_prediction_naive(user, item, self.rating_mat,
                                                                            self.played_mat, self.avg_rating_mat)
        user_recs_naive = np.nan_to_num(user_recs_naive)

        corrs_cache = {}
        user_recs_result = np.zeros((self.no_of_users, self.no_of_items))
        for user in range(self.no_of_users):
            user_recs_result[user,] = recs.item_to_item_prediction(self.config, user, self.rating_mat,
                                                                   self.played_mat, self.avg_rating_mat, corrs_cache)
        nptest.assert_array_almost_equal_nulp(user_recs_naive, user_recs_result, 126)


    def test_get_correlations_user_to_user(self):

        def test_pearson_correlation(rating_mat, avg_rating_mat, user1, user2):
            numerator = 0
            for i in range(rating_mat.shape[1]):
                if rating_mat[user1, i] > 0 and rating_mat[user2, i] > 0:
                    numerator += (rating_mat[user1, i] - avg_rating_mat[user1, i]) * (rating_mat[user2, i] - avg_rating_mat[user2, i])
            denominator1 = 0
            denominator2 = 0
            for i in range(rating_mat.shape[1]):
                if rating_mat[user1, i] > 0 and rating_mat[user2, i] > 0:
                    denominator1 += pow((rating_mat[user1, i] - avg_rating_mat[user1, i]), 2)
                    denominator2 += pow((rating_mat[user2, i] - avg_rating_mat[user2, i]), 2)
            denominator = math.sqrt(denominator1 * denominator2)
            if denominator == 0:
                return 0
            else:
                return numerator / denominator

        self.config["algorithm"] = "user_to_user"

        user_corr_naive = np.zeros((self.no_of_users, self.no_of_users))
        for user1 in range(self.rating_mat.shape[0]):
            for user2 in range(self.rating_mat.shape[0]):
                user_corr_naive[user1, user2] = test_pearson_correlation(self.rating_mat, self.avg_rating_mat, user1, user2)

        user_corrs = np.zeros((self.no_of_users, self.no_of_users))
        for user in range(self.no_of_users):
            corrs = recs.get_correlations(self.config, user, self.rating_mat, self.played_mat, self.avg_rating_mat)
            user_corrs[user, ] = corrs
        nptest.assert_array_almost_equal_nulp(user_corr_naive, user_corrs, 3)


    def test_get_correlations_item_to_item(self):

        def test_pearson_correlation(rating_mat, avg_rating_mat, item1, item2):
            numerator = 0
            for u in range(rating_mat.shape[0]):
                if rating_mat[u, item1] > 0 and rating_mat[u, item2] > 0:
                    if avg_rating_mat[u, item1] == 0 or avg_rating_mat[u, item2] == 0:
                        self.fail("Average rating matrix value was zero when not expected.")
                    numerator += (rating_mat[u, item1] - avg_rating_mat[u, item1]) * (rating_mat[u, item2] - avg_rating_mat[u, item2])
            denominator1 = 0
            denominator2 = 0
            for u in range(rating_mat.shape[0]):
                if rating_mat[u, item1] > 0 and rating_mat[u, item2] > 0:
                    denominator1 += pow((rating_mat[u, item1] - avg_rating_mat[u, item1]), 2)
                    denominator2 += pow((rating_mat[u, item2] - avg_rating_mat[u, item2]), 2)
            denominator = math.sqrt(denominator1 * denominator2)
            if denominator == 0:
                return 0
            else:
                return numerator / denominator

        self.config["algorithm"] = "item_to_item"

        item_corr_naive = np.zeros((self.no_of_items, self.no_of_items))
        for item1 in range(self.rating_mat.shape[1]):
            for item2 in range(self.rating_mat.shape[1]):
                item_corr_naive[item1, item2] = test_pearson_correlation(self.rating_mat, self.avg_rating_mat, item1, item2)

        item_corrs = np.zeros((self.no_of_items, self.no_of_items))
        for item in range(self.no_of_items):
            corrs = recs.get_correlations(self.config, item, self.rating_mat, self.played_mat, self.avg_rating_mat)
            item_corrs[item, ] = corrs
        nptest.assert_array_almost_equal_nulp(item_corr_naive, item_corrs, 2)
