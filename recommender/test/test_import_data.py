import unittest
from recommender.datareader import import_data as imp
from recommender.config import config as conf


class testImport(unittest.TestCase):

    def setUp(self):
        self.config = conf.get_config("unittest")

    # More extensive tests are done also to dataframe importing at test_prepare_data.py
    def test_load_data(self):
        columns_expected = ['yle_selva', 'c12', 'program_id', 'ns_st_pt', 'avg_yle_truelength', 'min_collectorreceived']
        rows_expected = 19
        try:
            df = imp.load_data(self.config)
            df = df.compute()
            columns_result = df.columns.values.tolist()
            rows = df.shape[0]
            self.assertTrue(columns_expected == columns_result and rows == rows_expected)
        except Exception:
            self.fail("test_load_data raised Exception")
