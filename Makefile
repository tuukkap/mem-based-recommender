VENV_NAME?=venv

develop: venv
	./$(VENV_NAME)/bin/python3 setup.py develop
.PHONY: develop

clean:
	$(RM) -r $(VIRTUALENV)
	$(RM) -r *.egg-info
.PHONY: clean

venv: $(VENV_NAME)/bin/activate
$(VENV_NAME)/bin/activate:
	python3 -m pip install virtualenv
	test -d $(VENV_NAME) || virtualenv -p python3 $(VENV_NAME)
	./$(VENV_NAME)/bin/pip install --upgrade pip # newer pip required by numpy and scipy install
	./$(VENV_NAME)/bin/pip install --upgrade "setuptools>=34" # newer setuptools required by dask
