# Memory-based recommender for Master's Thesis about recommendation engines

This repository contains memory-based recommendation engine for Master's Thesis about
recommendation engines for "Yle Lasten Areena".

Unfortunately the source data can't be shared publicly as it is real users' real data.
The purpose of this repository is to give an idea about the algorithms used.

## Requirements

* Linux or OSX environment with sudo rights.
* Lots of memory. 32 GB should be enough but you might get along with 16 GB also.
* Python 3.x

All commands below are expected to be run at the project root.

## Installing to a server

Edit the server name and install directory at the server to the deploy.sh file.
The deploy script creates a virtual environment under the install directory and installs
required modules to the virtual environment.
Run the deploy script:

    ./deploy.sh

The deploy script expects to have access to the server using ssh key. It copies source files to the server, runs Makefile (make develop) which runs setup.py to setup the virtual environment.

## Installing locally

Run:

    make develop
    
The command above creates virtual environment to the directory in question and installs
required modules to the virtual environment.
    
## Source data

The application expects to find the source data (viewing data and metadata) at "data/train" under projects root.
Unfortunately the source data can't be shared publicly. From test_resources you can get an idea what form the source data is.

Edit the file paths (with wildcards) to config/config_prod.json.

## Usage

### Running the API

Run the Recommendations API:

    venv/bin/run_api
    
You can use the following command line arguments:

* --use-sample-data forces the API to use only small part of the viewing data. As computing the whole dataset takes a while (several minutes), this is handy for testing purposes.
* With --algorithm you can select the recommendation algorithm. Choices are "user_to_user_1", "user_to_user_2" or "item_to_item". Default is "user_to_user_2" if this argument is not given.

An example with the arguments:

    venv/bin/run_api --use-sample-data --algorithm="user_to_user_1"
    
If you use the full dataset (e.g. run api without --use-sample-data), computing of the source data takes several minutes. When you see this line at the mem-rec-flask.log:

    INFO - root - Flask server should be now running.

then the API is ready to serve requests.
    
### Calling the API

How to call the API:

    http://localhost:5000/?user_id=[user_id]&limit=[number_of_recommendations]&user_type=[user_type]&recommender_type=[recommender_type]
    
* [user_id] is the ID of the user, e.g. "userid1".
* [number_of_recommendations] is the amount of given recommendations, e.g. "20".
* [user_type] is the type of the user. Choices are "desktop" or "mobile".
* [recommender_type] is the type of the recommendation method. Choices are "memory-based", "random" or "popularity". The memory based recommendation algorithm works with value "memory-based".

Replace localhost with the hostname where the API is installed.

Here's an example:

    http://localhost:5000/?user_id=userid1&limit=20&user_type=mobile&recommender_type=memory-based

The response is json.

### Creating statistics reports

You can create a report from the source data by calling:

    venv/bin/data_stats
    
You can create a report from the source data correlations by calling:

    venv/bin/corr_stats

If you get an error message:

    ImportError: Python is not installed as a framework

create a file ~/.matplotlib/matplotlibrc with backend parameter: "backend:TkAgg"

## License

See the LICENSE file at the root of this repository for license rights and limitations (EPL 1.0).
