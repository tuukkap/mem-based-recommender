#!/bin/bash

# Check current dir
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Edit these to your liking
INSTALL_DIR=/some/dir
SERVER_NAME=some-server
USER_NAME=some-user

echo "Deleting previous install..."

ssh $SERVER_NAME "
cd $INSTALL_DIR
rm -rf recommender
rm -rf config
rm -f Makefile
rm -f setup.py
rm -f README.md
exit
"

echo "Moving files to server..."

rsync -z -r --exclude '*.log' --exclude '$DIR/recommender/test' --exclude '$DIR/recommender/__pycache__' $DIR/setup.py $DIR/Makefile $DIR/README.md $DIR/config $DIR/recommender $USER_NAME@$SERVER_NAME:$INSTALL_DIR

echo "Running Makefile..."

ssh $SERVER_NAME "
cd $INSTALL_DIR
touch __init__.py
make develop
exit"

echo "Done!"
