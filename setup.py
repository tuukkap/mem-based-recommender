from setuptools import setup, find_packages


setup(
    name='mem-based-recommender',
    version='0.1',
    packages=find_packages(),
    install_requires=[
        'dask[complete]==1.0.0',
        'Flask==1.0.2',
        'Flask-RESTful==0.3.6',
        'matplotlib==3.0.2',
        'numpy==1.15.4',
        'pandas==0.23.4',
        'udatetime==0.0.16',
        'pytz==2018.5',
        'scipy==1.1.0',
        'brokenaxes==0.3.1'
    ],
    entry_points = {
        'console_scripts': ['run_api = recommender.api.run_api:main',
                            'data_stats = recommender.utils.data_statistics:main',
                            'corr_stats = recommender.utils.correlation_statistics:main']
    }
)
